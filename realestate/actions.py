# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


import json
import logging
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import (
    EventType,
)
from rasa_sdk.executor import CollectingDispatcher


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class ActionExtraction(Action):
    """Trích xuất thực thể"""

    def name(self):
        return "action_extraction"

    def run(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[EventType]:
        lastest_mess = tracker.latest_message

        dispatcher.utter_message(text=json.dumps(lastest_mess))

        return []
