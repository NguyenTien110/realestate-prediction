rasa test nlu -m  model_unsegmented/run_1/50%_exclusion/config-fasttext.tar.gz --out test_unsegmented_FT -u data/conversation_tests_no_segment.md
rasa test nlu -m  model_unsegmented/run_1/50%_exclusion/config.tar.gz --out test_unsegmented_WP -u data/conversation_tests_no_segment.md


rasa test nlu -m  model_fasttext/run_1/50%_exclusion/config-fasttext.tar.gz --out test_pyvi_FT -u data/conversation_tests_pyvi.md
rasa test nlu -m  model_fasttext/run_1/50%_exclusion/config.tar.gz --out test_pyvi_WP -u data/conversation_tests_pyvi.md

rasa test nlu -m  model_bert/run_1/50%_exclusion/config-bert.tar.gz --out test_vncore_Bert -u data/conversation_tests_vncore.md
rasa test nlu -m  model_bert/run_1/50%_exclusion/config.tar.gz --out test_vncore_WP -u data/conversation_tests_vncore.md


# rasa test nlu -m result_bert/run_1/75%_exclusion/config-bert.tar.gz  --out test_vncore_segmented_bert -u tests/conversation_tests_bert.md
# rasa test nlu -m result_bert/run_1/75%_exclusion/config.tar.gz  --out test_vn_core_segmented_bert -u tests/conversation_tests_bert.md


# rasa test nlu -m results_segment/run_1/75%_exclusion/config-fasttext.tar.gz  --out test_pyvi_segmented_fasttext -u tests/conversation_tests_pyvi.md


#compare mode

# CUDA_VISIBLE_DEVICES=0 rasa test nlu -u data/nlu_no_segment.md --out model_unsegmented -c config-bert.yml config-fasttext.yml config.yml -r 1 -p 0 50
# CUDA_VISIBLE_DEVICES=0 rasa test nlu -u data/nlu_pyvi.md --out model_fasttext -c  config-fasttext.yml config.yml -r 1 -p 0 50
# CUDA_VISIBLE_DEVICES=0 rasa test nlu -u data/nlu_vncore.md --out model_bert -c config-bert.yml config.yml -r 1 -p 0 50