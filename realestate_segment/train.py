import os
import sys

sys.stdout = open(sys.argv[1],'w')
print('hello world')
train_command = 'rasa test nlu --config config-bert.yml config-fasttext.yml'
train_bert = 'CUDA_VISIBLE_DEVICES=0 rasa train nlu --c config-bert.yml --out model-bert'
train_fasttext = 'CUDA_VISIBLE_DEVICES=0 rasa train nlu --c config-fasttext.yml --out model-fasttext'
os.system(train_command)
sys.stdout.close()