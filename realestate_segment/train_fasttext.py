
from rasa.nlu.model import Metadata, Interpreter

from rasa.nlu.training_data import load_data
from rasa.nlu.config import RasaNLUModelConfig
from rasa.nlu.model import Trainer
from rasa.nlu import config
from rasa.nlu.model import Metadata, Interpreter
import rasa
import time

# interpreter = Interpreter.load(
#     'models/test_sampling/config_file/config3-rc-dense/finalSamping_train125/nlu')

print(rasa.__file__)


def train (data, config_file, model_dir):
    training_data = load_data(data)
    trainer = Trainer(config.load(config_file))
    trainer.train(training_data)
    model_directory = trainer.persist(model_dir, fixed_model_name = 'nlu')

def ask_question(interpreter, text):
    return interpreter.parse(text)

train("data/nlu_pyvi.md","config-fasttext.yml",'models/fasttext/')
# interpreter = Interpreter.load(
#     'models/test_sampling/config_file/config3-rc-dense/overSamping_train125/nlu')
# #interpreter_list = [[Interpreter.load(
# #    f'models/ensemble/config3-rc-dense/finalSamping_train_{i}_{j}/nlu') for j in range(3)] for i in range(5)]
# # with open('default_test/default_context.txt','r') as f:
# #     texts = [i.replace("'",'"') for i in f.read().split('\n') if i]
# # print(len(texts))
# while(1):
#     start = time.time()
#     sent =  input('input sentence:')
#     # for sent in texts:
#     print(ask_question(interpreter,sent))
#     # print((time.time()-start)/len(texts))
#     #for i in range(5):
#     #    for j in range(3):
#     #        print(ask_question(interpreter_list[i][j], sent))

# # print(rasa.__file__)
