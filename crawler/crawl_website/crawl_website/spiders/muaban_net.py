import datetime
import logging

import scrapy
import time


class MuaBanNetSpider(scrapy.Spider):
    name = 'muaban_net'
    allowed_domains = ['muaban.net']
    base_url = 'https://muaban.net'
    start_urls = [
        base_url + '/ban-nha-ha-noi-l24-c32',
        base_url + '/ban-nha-ho-chi-minh-l59-c32'
    ]

    def parse(self, response, *args, **kwargs):
        list_post = response.xpath('//a[contains(@class, "list-item__link")]')

        for post in list_post:
            detail_page = response.urljoin(post.xpath('@href').get())
            detail = post.xpath('div[contains(@class, "list-item__content")]')
            title = detail.xpath('h2/text()').get()
            if 'số ' not in title:
                continue
            price = detail.xpath('div[@class="list-item__price-extension"]/span[@class="list-item__price"]/text()').get()
            area = detail.xpath('div[@class="list-item__price-extension"]/span[@class="list-item__area"]/b/text()').get()
            address = detail.xpath('div[@class="list-item__location-date"]/span[@class="list-item__location"]/text()').getall()
            time_update = detail.xpath('div[@class="list-item__location-date"]/span[@class="list-item__date"]/text()').getall()
            if price:
                price = price.replace('đ', '').strip()
            if area:
                area = area.replace(' m', '').replace(',', '.').strip()
            province = '',
            district = ''
            if address:
                try:
                    address = ' '.join(address).replace('\n', '').split(', ')
                    district = address[0].strip(),
                    province = address[1].strip()
                except Exception as e:
                    logging.error(e)
                    logging.error(address)
            if time_update:
                time_update = time_update[1].replace('\n', '')
            params = dict(price=price, title=title, province=province, district=district, area=area,
                          time_update=time_update)
            yield scrapy.Request(url=detail_page, callback=self.parse_page, cb_kwargs=params)
            time.sleep(1)

        next_page = response.xpath('//*[@id="next-link"]/@href').extract_first()
        if next_page:
            logging.info(next_page)
            next_page = response.urljoin(next_page)
            yield scrapy.Request(url=next_page, callback=self.parse, dont_filter=True)
        time.sleep(5)

    def parse_page(self, response, price, title, district, province, area, time_update):
        data = dict(
            source=response.request.url,
            title=title,
            description=''.join(response.xpath('//div[@class="body-container"]/text()').getall()),
            base_url=self.base_url,
            image=[],
            type_real_estate=response.xpath('//*[@property="name"]/text()').getall()[-1],
            type_post=None,
            area=area,
            price=price,
            district=district,
            province=province,
            info_contact=None,
            time_update=time_update,
            time_crawl=datetime.datetime.now()
        )
        info_contact = response.xpath('//div[@class="user-info__content"]')
        if info_contact is not None:
            data['info_contact'] = {
                "name": info_contact.xpath('div[@class="user-info__fullname"]/text()').get().replace('\n', ''),
                "phone": info_contact.xpath('div[@class="mobile-container__value"]/span/@mobile').get(),
            }
        logging.info(data)
        yield data
