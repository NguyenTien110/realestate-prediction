import datetime
from urllib.parse import urlencode

import requests
import scrapy


class MuaBanChinhChuNetSpider(scrapy.Spider):
    name = 'muabanchinhchu_net'
    allowed_domains = ['muabanchinhchu.net']
    base_url = 'https://muabanchinhchu.net'
    start_urls = [
        base_url
    ]

    def parse(self, response, *args, **kwargs):
        list_post = response.xpath('//div[@class="list-realty"]/ul/li/a/@href')

        for post in list_post:
            detail_page = response.urljoin(post.get())
            yield scrapy.Request(url=detail_page, callback=self.parse_page)

        next_page = response.xpath('//a[@class="next page-numbers"]/@href').extract_first()
        if next_page:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(url=next_page, callback=self.parse, dont_filter=True)

    def parse_page(self, response):
        data = dict(
            source=response.request.url,
            title=response.xpath('//h1/text()').get().strip(),
            description=''.join(response.xpath('//div[@class="realty-infor"]/p/text()').getall()),
            base_url=self.base_url,
            image=[],
            type_real_estate=None,
            type_post=None,
            area=response.xpath('//div[@class="price-area"]/span[@class="value"]/text()').get().strip() + '2',
            price=response.xpath('//span[@class="value price"]/text()').get().strip(),
            address=response.xpath('//div[@class="location-realty"]/a/text()').getall(),
            info_contact=None,
            time_update=response.xpath('//div[@class="ngayup"]/text()').get().strip()[-10:],
            time_crawl=datetime.datetime.now()
        )

        post_id = int(
            response.xpath('//link[@rel="shortlink"]/@href').get().replace('https://muabanchinhchu.net/?p=', ''))
        response_contact = self.get_phone(urlencode(dict(action="show_phone", post_id=post_id)))

        if response_contact.status_code == 200:
            data['info_contact'] = {"phone": response_contact.json()["phone"]}
        else:
            data['info_contact'] = {"phone": None}

        yield data

    def get_phone(self, payload):
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        return requests.post(url='https://muabanchinhchu.net/wp-admin/admin-ajax.php', headers=headers, data=payload)
