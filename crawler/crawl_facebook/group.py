from facebook_scraper import get_posts
import csv
import os
from dotenv import load_dotenv
load_dotenv()

list_group = ['dautubatdongsanvietnam', 'BATDONGSANVIETNAM88', 'dautubatdongsan.vn']


def crawl_group():
    data = []
    for group in list_group:
        list_posts = get_posts(group=group, credentials=os.getenv('CREDENTIALS_FACEBOOK'), pages=100)
        for post in list_posts:
            data.append(post)
    with open('data/facebook.csv', 'w') as f:
        writer = csv.writer(f)
        for e in data:
            writer.writerow(e)
        f.close()


if __name__ == '__main__':
    crawl_group()
