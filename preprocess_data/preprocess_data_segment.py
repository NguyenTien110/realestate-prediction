from bs4 import BeautifulSoup
import csv
import re
import sys
from pyvi import ViTokenizer, ViPosTagger

csv.field_size_limit(sys.maxsize)

def clean_html(raw_html):
  cleanr = re.compile('<.*?>')
  cleanr = re.sub(cleanr, ' ', raw_html)
  return cleanr

# with open('export_100000.csv', 'r') as fr:
#     data = fr.readlines()

# with open('export_clean.csv', 'w') as fw:
#     for line in data:
#         line = line.replace('&lt;', '<')
#         line = line.replace('&gt;', '>')
#         fw.write(clean_html(line))

# # normalize
# from text_normalize import preprocess_text
# def write_normal_text():
#     with open('export_clean.csv', 'r') as f:
#         data = f.readlines()
#         with open('export_clean_1.csv', 'w') as f:
#             for line in data:
#                 f.write(preprocess_text(line) + '\n')

# def write_remove_punctiation():
#     print('done 1')
#     LEGAL = " \"'()*+,-./0123456789:;<=>?@[\\]`abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ{|}~\n"
#     with open('export_clean_1.csv', 'r') as f:
#         data = f.readlines()
#         for i in range(len(data)):
#             for j in range(len(data[i])):
#                 if data[i][j].lower() not in LEGAL:
#                     data[i] = data[i].replace(data[i][j], ' ')
#         print('done 2')
#         with open('export_clean_2.csv', 'w') as f:
#             for line in data:            
#                 f.write(line.replace('"', "'"))

# write_normal_text()
# write_remove_punctiation()


# # create csv
# with open('export_clean_2.csv', 'r') as f:
#     data = f.read().split('@\n@')
#     for i in range(len(data)):
#         data[i] = data[i].split('@(----)@')
#         for j in range(len(data[i])):
#             data[i][j] = data[i][j].replace('\n', '. ')
#             if ',' in data[i][j]:
#                 data[i][j] = '"' + data[i][j] + '"'
#         data[i] = ','.join(data[i])
#     data = '\n'.join(data)

# with open('export_1.csv', 'w') as f:
#     f.write(data.replace('@', ''))

# # remove duplicated
# with open('export_1.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     tieude = []
#     data = []
#     for line in lines:
#         if len(line) < 10:
#             continue
#         if line[8] not in tieude:
#             tieude.append(line[8])
#             data.append(line)
#     print(len(tieude))
#     with open('export_2.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in data:
#             spamwriter.writerow(e)

# # remove missing quan huyen
# with open('export_2.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     data = []
#     for line in lines:
#         if line[4] != 0 and line[4] != '':
#             data.append(line)
#     with open('export_3.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in data:
#             spamwriter.writerow(e)

# # remove sample empty phuong xa, duong pho
# with open('export_3.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     empty = []
#     for line in lines:
#         if line[3] != 0 and line[3] != '' and line[5] != '' and line[5] != 0:
#             empty.append(line)

#     with open('export_4.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in empty:
#             spamwriter.writerow(e)

# # remove giatien = thoa thuan (khong ro rang)
# with open('export_4.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     data = []
#     rent = [452, 546, 547, 548, 549, 550, 551, 552]
#     sell = [457, 450, 554, 553, 555, 556, 557, 558, 559]
#     for line in lines:
#         if line[6] != 'Thỏa thuận' and line[7] != '':
#             data.append(line)
#     with open('export_5.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in data:
#             if int(e[1]) in rent:
#                 e[1] = 'rent'
#             else:
#                 e[1] = 'sell'
#             spamwriter.writerow(e)

# #remove link
# import re
# with open('export_5.csv', 'r') as f:
#     data = f.read()
#     data = re.sub(r'http\S+', '', data)
#     with open('export_6.csv', 'w') as fw:
#         fw.write(data)

# with open('export_6.csv', 'r') as f:
#     data = f.read()
#     with open('export_6.csv', 'w') as fw:
#         fw.write(clean_html(data))

# # P8 -> Phường 8, Q8 -> Quận 8
# with open('export_6.csv', 'r') as fr:
#     data = fr.readlines()
#     for i in range(len(data)):
#         data[i] = data[i].replace(' q12', ' quận 12')
#         data[i] = data[i].replace('q12 ', 'quận 12 ')
#         data[i] = data[i].replace(' q11', ' quận 11')
#         data[i] = data[i].replace('q11 ', 'quận 11 ')
#         data[i] = data[i].replace(' q10', ' quận 10')
#         data[i] = data[i].replace('q10 ', 'quận 10 ')
#         data[i] = data[i].replace(' q1', ' quận 1')
#         data[i] = data[i].replace('q1 ', 'quận 1 ')
#         data[i] = data[i].replace(' q2', ' quận 2')
#         data[i] = data[i].replace('q2 ', 'quận 2 ')
#         data[i] = data[i].replace(' q3', ' quận 3')
#         data[i] = data[i].replace('q3 ', 'quận 3 ')
#         data[i] = data[i].replace(' q4', ' quận 4')
#         data[i] = data[i].replace('q4 ', 'quận 4 ')
#         data[i] = data[i].replace(' q5', ' quận 5')
#         data[i] = data[i].replace('q5 ', 'quận 5 ')
#         data[i] = data[i].replace(' q6', ' quận 6')
#         data[i] = data[i].replace('q6 ', 'quận 6 ')
#         data[i] = data[i].replace(' q7', ' quận 7')
#         data[i] = data[i].replace('q7 ', 'quận 7 ')
#         data[i] = data[i].replace(' q8', ' quận 8')
#         data[i] = data[i].replace('q8 ', 'quận 8 ')
#         data[i] = data[i].replace(' q9', ' quận 9')
#         data[i] = data[i].replace('q9 ', 'quận 9 ')
#     with open('export_6.csv', 'w') as fw:
#         for line in data:        
#             fw.write(line)

# #remove lien he
# with open('export_6.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     with open('export_8.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in lines:
#             if len(e) < 11:
#                 continue
#             spamwriter.writerow(e[:-1])

# # replace , -> . in area, money
# with open('export_8.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     count = 0
#     # for e in lines:
#     #     new = e[6].replace(',', '.')
#     #     # print(e[6])
#     #     if e[6] not in e[9] and new not in e[9]:
#     #         count += 1
#     #         continue
#     #     new = e[7].replace(',', '.')
#     #     if e[7] not in e[9] and new not in e[9]:
#     #         count += 1
#     # print(count)
#     with open('export_9.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in lines:
#             if ',' in e[6]:
#                 new = e[6].replace(',', '.')
#                 if e[6] in e[9]:
#                     e[9] = e[9].replace(e[6], new)
#                     e[6] = new
#                 if new in e[9]:
#                     e[6] = new
#             if ',' in e[7]:
#                 new = e[7].replace(',', '.')
#                 if e[7] in e[9]:
#                     e[9] = e[9].replace(e[7], new)
#                     e[7] = new
#                 if new in e[9]:
#                     e[7] = new
#             spamwriter.writerow(e)


# # replace 5.5 -> 5'''5 in area and money
# with open('export_9.csv', encoding='utf-8') as f:
#     lines = csv.reader(f)
#     count = 0
#     with open('export_10.csv', 'w', newline='') as fa:
#         spamwriter = csv.writer(fa)
#         for e in lines:
#             if e[6] in e[9]:
#                 new = e[6].replace('.', "'''")
#                 e[9] = e[9].replace(e[6], new)
#                 e[6] = new
#             if e[7] in e[9]:
#                 new = e[7].replace('.', "'''")
#                 e[9] = e[9].replace(e[7], new)
#                 e[7] = new
#             spamwriter.writerow(e)


# def write_remove_punctiation_1():
#     LEGAL = " \"',.0123456789abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ\n"
#     with open('export_10.csv', 'r') as f:
#         data = f.readlines()
#         for i in range(len(data)):
#             for j in range(len(data[i])):
#                 if data[i][j].lower() not in LEGAL:
#                     data[i] = data[i].replace(data[i][j], ' ')
#         with open('export_11.csv', 'w') as f:
#             for line in data:
#                 f.write(line)

# write_remove_punctiation_1()

#remove strip
with open('../data/export_11.csv', encoding='utf-8') as f:
    lines = csv.reader(f)
    with open('../data/export_12.csv', 'w', newline='') as fa:
        spamwriter = csv.writer(fa)
        for e in lines:
            if 'đa c địa' in e[-1]:
                continue
            e[9] = e[9].replace('nbsp', ' ')
            e[-1] = e[-1].replace('",', '"')
            # e[-1] = e[-1].replace(',', ' ')
            # e[-1] = e[-1].replace('.', ' ')
            e[-1] = e[-1].replace('" ', '"')
            e[-1] = e[-1].replace(' "', '"')
            e[-1] = e[-1].strip()
            e[-1] = re.sub(' +', ' ', e[-1])
            e[3] = e[3].strip()
            e[4] = e[4].strip()
            e[5] = e[5].strip()
            e[6] = e[6].replace("'''", '.').strip()
            e[7] = e[7].replace("'''", '.').strip()
            e[9] = e[9].replace("'''", '.').strip()
            e[9] = ' '.join(e[9].split())
            e[8] = ' '.join(e[8].split())
            spamwriter.writerow(e)

def isfloat(str):
    try: 
        float(str)
    except ValueError: 
        return False
    return True

with open('../data/export_12.csv', encoding='utf-8') as f:
    lines = csv.reader(f)
    sell = []
    rent = []
    # next(lines)
    with open('../error.md', 'w') as fw:
        for line in lines:
            has_entity = 0
            temp = {}
            # handle province
            if ' ' + line[2] + ' ' in line[9]:
                temp['@1'] = ' [' + line[2] + '](province) '
                line[9] = line[9].replace(' ' + line[2] + ' ', '@1')
                has_entity = 1

            # handle district
            if isfloat(line[4]):
                if 'quận '+line[4] in line[9]:
                    temp['@2'] = ' [quận ' + line[4] + '](district) '
                    line[9] = line[9].replace(' quận ' + line[4], '@2')
                    has_entity = 1
                elif 'huyện ' + line[4] in line[9]:
                    temp['@2'] = ' [huyện ' + line[4] + '](district) '
                    line[9] = line[9].replace(' huyện ' + line[4], '@2')
                    has_entity = 1
                else:
                    fw.write(line[0] + ' district\n')
            else:
                temp['@2'] = ' [' + line[4] + '](district) '
                line[9] = line[9].replace(' ' + line[4], '@2')
                has_entity = 1

            # handle street
            if line[3] != line[4]:
                if isfloat(line[3]):
                    if 'đường '+line[3] in line[9]:
                        temp['@3'] = ' [đường ' + line[3] + '](street) '
                        line[9] = line[9].replace(' đường ' + line[3], '@3')
                        has_entity = 1
                    elif 'ql' + line[3] in line[9]:
                        temp['@3'] = ' [quốc lộ ' + line[3] + '](street) '
                        line[9] = line[9].replace(' ql' + line[3], '@3')
                        has_entity = 1
                    elif 'quốc lộ ' +line[3] in line[9]:
                        temp['@3'] = ' [quốc lộ ' + line[3] + '](street) '
                        line[9] = line[9].replace(' quốc lộ ' + line[3], '@3')
                        has_entity = 1
                    else:
                        fw.write(line[0] + ' street\n')
                else:
                    temp['@3'] = ' [' + line[3] + '](street) '
                    line[9] = line[9].replace(' ' + line[3], '@3')
                    has_entity = 1

            # handle village
            if line[5] != line[4] and line[5] != line[3]:
                if isfloat(line[5]):
                    if 'phường '+line[5] in line[9]:
                        has_entity = 1
                        temp['@4'] = ' [phường ' + line[5] + '](village) '
                        line[9] = line[9].replace(' phường ' + line[5], '@4')
                    elif 'xã ' + line[5] in line[9]:
                        has_entity = 1
                        temp['@4'] = ' [xã ' + line[5] + '](village) '
                        line[9] = line[9].replace(' xã ' + line[5], '@4')
                    else:
                        fw.write(line[0] + ' village: ' + line[5] + '\n')
                else:
                    has_entity = 1
                    temp['@4'] = ' [' + line[5] + '](village) '
                    line[9] = line[9].replace(' ' + line[5], '@4')

            # handle money
            if isfloat(line[6]):
                if line[6] + ' tỷ' in line[9]:
                    has_entity = 1
                    temp['@5'] = ' [' + line[6] + ' tỷ](money) '
                    line[9] = line[9].replace(line[6] + ' tỷ', '@5')
                elif line[6] + ' triệu' in line[9]:
                    has_entity = 1
                    temp['@5'] = ' [' + line[6] + ' triệu](money) '
                    line[9] = line[9].replace(line[6] + ' triệu', '@5')
                # else:
                #     print(line[0] + ' isnumeric money: ' + line[6])
            elif ' ' + line[6] + ' ' in line[9]:
                has_entity = 1
                temp['@5'] = ' [' + line[6] + '](money) '
                line[9] = line[9].replace(' ' + line[6] + ' ', '@5')
        
            # handle area
            if isfloat(line[7]):
                if line[7] + 'm2' in line[9]:
                    has_entity = 1
                    temp['@6'] = ' [' + line[7] + 'm2](area) '
                    line[9] = line[9].replace(line[7] + 'm2', '@6')
                elif line[7] + 'm' in line[9]:
                    has_entity = 1
                    temp['@6'] = ' [' + line[7] + 'm](area) '
                    line[9] = line[9].replace(line[7] + 'm', '@6')
                # else:
                #     print(line[0] + ' isnumeric area: ' + line[6])
            elif ' ' + line[7] + ' ' in line[9]:
                has_entity = 1
                temp['@6'] = ' [' + line[7] + '](area) '
                line[9] = line[9].replace(' ' + line[7] + ' ', '@6')

            for e in temp.keys():
                line[9] = line[9].replace(e, temp[e])

            if has_entity:
                if line[1]== 'sell':
                    sell.append(' '.join(line[9].split()))
                else:
                    rent.append(' '.join(line[9].split()))
sell = list(set(sell))
rent = list(set(rent))

print("sell: " + str(len(sell)))
print("rent: " + str(len(rent)))

sell_x=[]
rent_x=[]

sell_y = []
rent_y = []

for line in sell:
    if 200<len(line)<800 and "http" not in line and line.count('](')>3:
        sell_x.append(line)
    elif len(line) >= 150 and len(line) <= 200 and line.count('](')>3:
        sell_y.append(line)
    elif len(line) >= 800 and len(line) <= 900 and line.count('](')>3:
        sell_y.append(line)

for line in rent:
    if 200<len(line)<800 and "http" not in line and line.count('](')>3:
        rent_x.append(line)
    elif len(line) >= 150 and len(line) <= 200 and line.count('](')>3:
        rent_y.append(line)
    elif len(line) >= 800 and len(line) <= 1000 and line.count('](')>3:
        rent_y.append(line)

sell_y = sell_y + sell_x[-900:]
rent_y = rent_y + rent_x[-100:]
sell_x = sell_x[:-900]
rent_x = rent_x[:-100]

print("sell_x: " + str(len(sell_x)))
print("rent_x: " + str(len(rent_x)))
print("sell_y: " + str(len(sell_y)))
print("rent_y: " + str(len(rent_y)))

with open('tinhthanh_viet_hoa.csv', encoding='utf-8') as f:
    lines = csv.reader(f)
    tinhthanh = {}
    for line in lines:
        tinhthanh[(line[1] + ' ' + line[0]).strip()] = line[2]


from vncorenlp import VnCoreNLP
rdrsegmenter = VnCoreNLP("./vncorenlp/VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')

def segment(comment_text):
    # vncorenlp
    # sentences = rdrsegmenter.tokenize(comment_text)
    # comment_text = ". ".join([' '.join(sent)for sent in sentences])

    # pyvi
    comment_text = ViTokenizer.tokenize(comment_text)
    
    comment_text = comment_text.replace(" ]", "]")
    comment_text = comment_text.replace("[ ", "[")
    comment_text = comment_text.replace("( ", "(")
    comment_text = comment_text.replace(" )", ")")
    comment_text = comment_text.replace(" _ ", "_")
    comment_text = comment_text.replace("] (", "](")
    comment_text = comment_text.replace("_]", "]")
    return comment_text


import re
def replace_tinhthanh(line: str):
    temp = {}
    temp1 = {}
    count = 1
    index = 1
    regex = re.findall('\[[\w\s]*\]\(\w*\)', line)
    for e in regex:
        line = line.replace(e, '@@' + str(index))
        temp1['@@' + str(index)] = e
        index += 1
    for e in tinhthanh.keys():
        if ' ' + e + ' ' in line:
            temp['#' + str(count)] = ' [' + e + '](' + tinhthanh[e] + ')'
            line = line.replace(' ' + e + ' ', ' #' + str(count) + ' ')
            count += 1
    for key in temp1.keys():
        line = line.replace(key + ' ', temp1[key] + ' ')
        if line.find(key) == len(line) - len(key):
            line = line.replace(key, temp1[key])
    for key in temp.keys():
        line = line.replace(key + ' ', temp[key] + ' ')
    return segment(' '.join(line.split()))

type_segment = 'pyvi'
with open('realestate_segment/data/nlu_' + type_segment + '.md', 'w') as f:
    f.write('## lookup: district\ndata/district.txt\n')
    f.write('## lookup: village\ndata/village.txt\n')
    f.write('## lookup: street\ndata/street.txt\n')
    f.write('## lookup: province\ndata/province.txt\n')
    f.write('## intent: bán\n')
    count = 1
    for line in sell_x:
        f.write('- ' + replace_tinhthanh(line) + '\n')
        count += 1
        # if count == 100: break
    f.write('## intent: thuê\n')
    for line in rent_x:
        f.write('- ' + replace_tinhthanh(line) + '\n')

#test
with open('realestate_segment/tests/conversation_tests_'+ type_segment + '.md', 'w') as f:
    f.write('## intent: bán\n')
    for i,s in enumerate(sell_y):
        f.write('- ' + replace_tinhthanh(s) + '\n')
        # f.write('  - action_extraction\n')

    f.write('## intent: thuê\n')
    for i,s in enumerate(rent_y):
        f.write('- ' + replace_tinhthanh(s) + '\n')
        # f.write('  - action_extraction\n')
