from bs4 import BeautifulSoup
import re
import html
from pyvi import ViTokenizer
from vncorenlp import VnCoreNLP
from preprocess_data import VietnameseTextNormalizer


def clean_html(raw_html):
    # remove html tag VD: (<p>)
    cleanr = re.compile('<.*?>')
    cleanr = re.sub(cleanr, ' ', raw_html)

    # replace html symbol VD: (&lt;)
    cleanr = html.unescape(cleanr)

    return cleanr


# normalize
def normalize(string: str):
    # # example
    # print('hiếu thảo' == 'hiếu thảo')
    # print(VietnameseTextNormalizer.Normalize('hiếu thảo') == 'hiếu thảo')
    # print(VietnameseTextNormalizer.Normalize('hoà thuận'))

    return VietnameseTextNormalizer.Normalize(string).replace('\n', '. ')


# remove icon 😆😁😀🤣😍😡😭
def remove_icon(string: str):
    LEGAL = " \"'()*,-./0123456789:;<=>?@[\\]`abcdefghijklmnopqrstuvwxyzáàảãạăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ{|}~\n"
    for j in range(len(string)):
        if string[j].lower() not in LEGAL:
            string = string.replace(string[j], ' ')
    return string


# remove link
def remove_link(string: str):
    return re.sub(r'http\S+', '', string)


# option segment
# rdrsegmenter = VnCoreNLP("./vncorenlp/VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')
def segment(comment_text: str, type_segment='vncorenlp'):
    # vncorenlp
    if type_segment == 'vncorenlp':
        sentences = rdrsegmenter.tokenize(comment_text)
        comment_text = ". ".join([' '.join(sent) for sent in sentences])
    elif type_segment == 'pyvi':
        # pyvi
        comment_text = ViTokenizer.tokenize(comment_text)
    else:
        return comment_text

    comment_text = comment_text.replace(" ]", "]")
    comment_text = comment_text.replace("[ ", "[")
    comment_text = comment_text.replace("( ", "(")
    comment_text = comment_text.replace(" )", ")")
    comment_text = comment_text.replace(" _ ", "_")
    comment_text = comment_text.replace("] (", "](")
    comment_text = comment_text.replace("_]", "]")

    return comment_text


def preprocess(string: str, type_segment='none'):
    clean_string = clean_html(string)
    clean_string = remove_icon(clean_string)
    clean_string = remove_link(clean_string)
    clean_string = normalize(clean_string)
    # clean_string = segment(clean_string, type_segment)
    return ' '.join(clean_string.split())


if __name__ == "__main__":
    example = '''<DIV xss=removed id=input_line_0><SPAN xss=removed id=input_part_0 data-mention="Nhà (3.2*12m) lửng, 3lầu mặt tiền Hưng Phú P8 Q8">Nhà (3.2*12m) lửng, 3lầu mặt tiền Hưng Phú P8 Q8</SPAN></DIV>
<DIV xss=removed id=input_line_1><SPAN xss=removed id=input_part_0 data-mention="+ Diện tích: 3.2 x 12m ~ 37.6m2, sàn 131m2">+ Diện tích: 3.2 x 12m ~ 37.6m2, sàn 131m2</SPAN></DIV>
<DIV xss=removed id=input_line_2><SPAN xss=removed id=input_part_0 data-mention="+ Kết cấu: Nhà xây dựng kiên cố, trệt, lửng, 3 lầu">+ Kết cấu: Nhà xây dựng kiên cố, trệt, lửng, 3 lầu</SPAN></DIV>
<DIV xss=removed id=input_line_3><SPAN xss=removed id=input_part_0 data-mention="+ Vị trí: Gần cầu Chữ Y, kế bên quận 5, quận 1, dễ dàng di chuyển qua các quận 4, 7, ……">+ Vị trí: Gần cầu Chữ Y, kế bên quận 5, quận 1, dễ dàng di chuyển qua các quận 4, 7, ……</SPAN></DIV>
<DIV xss=removed id=input_line_4><SPAN xss=removed id=input_part_0 data-mention="+ Tiện ích: Khu buôn bán kinh doanh sầm uất, gần chợ, siêu thị, khu ăn uống, mua sắm hoạt động 24/7 trong bán kính 100m.">+ Tiện ích: Khu buôn bán kinh doanh sầm uất, gần chợ, siêu thị, khu ăn uống, mua sắm hoạt động 24/7 trong bán kính 100m.</SPAN></DIV>
<DIV xss=removed id=input_line_5><SPAN xss=removed id=input_part_0 data-mention="+ Hướng Nam">+ Hướng Nam</SPAN></DIV>
<DIV xss=removed id=input_line_6><SPAN xss=removed id=input_part_0 data-mention="+ Sổ hồng hoàn công đủ">+ Sổ hồng hoàn công đủ</SPAN></DIV>
<DIV xss=removed id=input_line_7><SPAN xss=removed id=input_part_0 data-mention="+ Giá: 7.8 Tỷ (bớt lộc)">+ Giá: 7.8 Tỷ (bớt lộc)</SPAN></DIV>
<DIV xss=removed id=input_line_8><SPAN xss=removed id=input_part_0 data-mention="*** Hỗ Trợ Vay Ngân Hàng 70% Giá Trị Nhà ***">*** Hỗ Trợ Vay Ngân Hàng 70% Giá Trị Nhà ***</SPAN></DIV>
<DIV xss=removed id=input_line_9><SPAN xss=removed id=input_part_0 data-mention="Liên hệ: 0988 467 678 Mr Vũ">Liên hệ: 0988 467 678 Mr Vũ</SPAN></DIV>
<DIV xss=removed id=input_line_10><SPAN xss=removed id=input_part_0 data-mention="Web: www.muabannhasg.com">Web: www.muabannhasg.com</SPAN></DIV>
<DIV xss=removed id=input_line_11><SPAN xss=removed id=input_part_0 data-mention="Công ty Bất Động Sản Tâm Điền">Công ty Bất Động Sản Tâm Điền</SPAN></DIV>
<DIV xss=removed id=input_line_12><SPAN xss=removed id=input_part_0 data-mention="Địa chỉ: 774 đường Hưng Phú, Phường 10, Quận 8">Địa chỉ: 774 đường Hưng Phú, Phường 10, Quận 8</SPAN></DIV>
<DIV xss=removed id=input_line_13><SPAN xss=removed id=input_part_0 data-mention="Nhận Ký Gửi Nhà Đất Các Quận TP.HCM">Nhận Ký Gửi Nhà Đất Các Quận TP.HCM</SPAN></DIV>'''
    print('none: ', preprocess(example))
    print('vncorenlp: ', preprocess(example, 'vncorenlp'))
    print('pyvi: ', preprocess(example, 'pyvi'))
