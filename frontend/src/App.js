import React, {  useState } from 'react';
import './App.css';
import axios from 'axios';
function App() {
  const [output, setOutput]=useState('');
  const [text, setText]=useState('');
  const onChange= (event) => {
		setText(event.target.value)
	}
  
  const onSubmit = async (event) =>{
		event.preventDefault();
      
		try {

      const res = await axios({
         baseURL: "Sửa url tại đây",
         method: "POST",
         url: "/model/parse",

         data: {
          Text: text
          },
      });

      if (res.status == 200) {
          setOutput(res.data)
        
      }
   } catch (error) {
      alert("Đã có lỗi xảy ra")
     
    
   }
		
	}
	 


  return (
    <main className="mainContainer">
      <section className="textContainer">
        <h1 id="title">Web mining</h1>
       
      </section>

      <form  className="formContainer" id="writingZone" onSubmit={onSubmit}>
        <div className="cardContainer">
          <div className="card">
            <header className="col-12" id="languages">
              <div className="btnLangContainer col-5" id="one">
                <button className="heatherText enLang col-6" id="en1" type="button">Input</button>
              
              </div>

            
              <div className="arrowContainer col-2">
                <button type="button" value="&#8644;" id="alternate" title="alternate"><span className="doubleArrow">&#8644;</span></button>
              </div>

              <div className="btnLangContainer col-5" id="two">
              <button className="heatherText enLang col-6" id="en1" type="button">Output</button>
                
              </div>
            </header>

            <div id="divideSpace">
    
                <textarea id="enterText" name="enterText" rows="10" lang="en" spellcheck="true" value={text} onChange={onChange}  autofocus></textarea>
                <textarea id="outputText" name="outputText" rows="10" spellcheck="false" value={output} disabled> </textarea>
              
          </div>
        </div>

        <div className="submitContainer">
          <button className="col-3" type="submit" value="submit" id="textSubmit">submit</button>
        </div>
        </div>
      </form>
    </main>
  );
}

export default App;
