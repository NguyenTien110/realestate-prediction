import requests
from preprocess_data import VietnameseTextNormalizer
from flask import request, Flask, jsonify
from flask_cors import CORS
from preprocess_data.preprocess import preprocess
from vncorenlp import VnCoreNLP
from pyvi import ViTokenizer
import os
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
CORS(app)

# rdr_segmentor = VnCoreNLP("./vncorenlp/VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')


def segment(comment_text: str, type_segment='vncorenlp'):
    # vncorenlp
    if type_segment == 'vncorenlp':
        sentences = rdr_segmentor.tokenize(comment_text)
        comment_text = ". ".join([' '.join(sent) for sent in sentences])
    elif type_segment == 'pyvi':
        # pyvi
        comment_text = ViTokenizer.tokenize(comment_text)
    else:
        return comment_text

    comment_text = comment_text.replace(" ]", "]")
    comment_text = comment_text.replace("[ ", "[")
    comment_text = comment_text.replace("( ", "(")
    comment_text = comment_text.replace(" )", ")")
    comment_text = comment_text.replace(" _ ", "_")
    comment_text = comment_text.replace("] (", "](")
    comment_text = comment_text.replace("_]", "]")

    return comment_text


@app.route('/model/parse', methods=["POST"])
def text_summary():
    content = request.get_json()
    text = content['text']
    text = preprocess(text)
    text = segment(text, content.get('segment', 'vncorenlp'))
    text = VietnameseTextNormalizer.Normalize(text)
    print("text:", text)
    res = requests.post('http://localhost:1235/model/parse', json={"text": text})
    print(res.json())
    return jsonify({"content": res.json()})


if __name__ == '__main__':
    app.run(host=os.getenv('APP_HOST'), debug=True, port=os.getenv('APP_PORT'))
